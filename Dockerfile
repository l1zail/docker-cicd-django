FROM python:3-alpine

ENV PYTHONUNBUFFERED=1

WORKDIR /app
RUN mkdir db

COPY requirements.txt /app
RUN pip install -r requirements.txt

COPY . /app
VOLUME ["/app/db"]

EXPOSE 8000
CMD sh init.sh && python manage.py runserver 0.0.0.0:8000
